## Golang gRPC Client ##

## Init Golang Module ##

```bash
go mod init gitlab.com/pragmaticreviews/grpc-nodego
```

## Install GRPC ##

```bash
go get -u google.golang.org/grpc
```

## Generate Posts Service Stub ##

```bash
protoc -I ./services ./services/posts_service.proto --go_out=plugins=grpc:./services
```

## Node.js gRPC Server  ##

## Install gRPC ##

```bash
cd node-server
npm install --save grpc
```

## Run the gRPC Server (from node-server) ##

```bash
node index.js
```
